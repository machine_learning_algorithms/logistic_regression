# logistic regression 

# gradient descent 
# init theta
# calculate grad of cost function wrt theta
# update theta

import random 
import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
plt.ion()

# load data file
df = pd.read_csv('../data/cleveland_heart.csv')
df = df.replace('?',0)
df = df.apply(pd.to_numeric,errors='ignore')
# goal: 0 - 4 severity of heart disease; in binary: 0 vs. 1-4

class LogisticRegression():
    # logistic regression class
    def __init__(self,X,Y,addbias=True,learningRate=0.01,maxIter=1000):
        # init
        self.lr = learningRate
        self.maxIter = maxIter
        self.target = Y
        if addbias is True:
            self.data = self.bias_column(X)
        else:
            self.data = X
        self.numSamples, self.numFeatures = np.shape(self.data)
        self.theta = np.zeros(self.numFeatures) # init theta as ones
 
    def bias_column(self,X):
        # add bias column
        bias = np.ones([X.shape[0],1])
        return np.concatenate([bias,X],axis=1)

    def logistic_fxn(self,z):
        return 1 / ( 1 + np.exp( -z ) )

    def cost_fxn(self):
        # binary cross entropy 
        Ypred = self.logistic_fxn(np.dot( self.data,self.theta))
        cost = (-1.0/self.numSamples) * np.sum(self.target * np.log(Ypred) +
                (1-self.target) * (np.log(1 - Ypred)))
        return cost

    def gradient(self):
        Ypred = self.logistic_fxn(self.data)
        grad = (1.0/self.numSamples) * np.dot( (Ypred-self.target), self.data)
        return grad

    def train(self):
        # gradient descent
        for iterCount in range(self.maxIter):
            cost = self.cost_fxn()
            grad = self.gradient()
            self.theta = self.theta - self.lr * grad
            print('iteration %s : cost %s ' % (iterCount,cost))

    def predict(self,X):
       X = self.bias_column(X)
       Ypred = self.logistic_fxn(X)
       classPred = np.round(Ypred)
       return classPred

# TODO: scoring: f1, ROC, sig., ? Model evaluation
# multinomial regression / softmax regression / max entropy classifier
# regress against Kth outcome and setup logistic regression for K-1 outcomes
# solve for B coefficients using IRWLS or gradients


# split into train/val/test sets
train, test = train_test_split(df,test_size=0.2,random_state=1234)
train, val = train_test_split(train, test_size = 0.25,random_state = 8888)
# train
trainX = train.loc[:,train.columns!='num']
trainY = train['num']
trainY = (trainY > 0.1).astype(int)
logReg = LogisticRegression(trainX,trainY,learningRate=0.01,maxIter=1000)
logReg.train() 

# test
predYtest = logReg.classify(test[:,test.columns!='num'],test['num'])
# score
# f1 = f1_score(predYtest, test['num'], 1)

# stepwise regression: step forward / backward. The process is unstable wrt to
# data changes. Not a good approach. 
# start with constant variable term
